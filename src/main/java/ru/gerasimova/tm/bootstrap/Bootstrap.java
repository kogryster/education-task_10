package ru.gerasimova.tm.bootstrap;

import ru.gerasimova.tm.api.controller.ICommandController;
import ru.gerasimova.tm.api.controller.IProjectController;
import ru.gerasimova.tm.api.controller.ITaskController;
import ru.gerasimova.tm.api.repository.ICommandRepository;
import ru.gerasimova.tm.api.repository.IProjectRepository;
import ru.gerasimova.tm.api.repository.ITaskRepository;
import ru.gerasimova.tm.api.service.ICommandService;
import ru.gerasimova.tm.api.service.IProjectService;
import ru.gerasimova.tm.api.service.ITaskService;
import ru.gerasimova.tm.constant.ArgumentConst;
import ru.gerasimova.tm.constant.TerminalConst;
import ru.gerasimova.tm.controller.CommandController;
import ru.gerasimova.tm.controller.ProjectController;
import ru.gerasimova.tm.controller.TaskController;
import ru.gerasimova.tm.repository.CommandRepository;
import ru.gerasimova.tm.repository.ProjectRepository;
import ru.gerasimova.tm.repository.TaskRepository;
import ru.gerasimova.tm.service.CommandService;
import ru.gerasimova.tm.service.ProjectService;
import ru.gerasimova.tm.service.TaskService;
import ru.gerasimova.tm.util.TerminalUtil;



public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController =  new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController =  new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
