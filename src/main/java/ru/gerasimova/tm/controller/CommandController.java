package ru.gerasimova.tm.controller;

import ru.gerasimova.tm.api.controller.ICommandController;
import ru.gerasimova.tm.api.service.ICommandService;
import ru.gerasimova.tm.model.Command;
import ru.gerasimova.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Gerasimova");
        System.out.println("E-MAIL: kogryster@gmail.com");
    }

    public void exit() {System.exit(0); }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandService.getArguments();
        System.out.println(Arrays.toString(arguments));
    }

    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemory);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

}
